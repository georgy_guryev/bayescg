function [x_m, dx, tr_Sm, Sigma_F, nu, norm_vec, S, S_norm, Y, Y_norm, a_m, a_m_inv] =  BayesCG(A, b, x_0, Sigma_0, epsilon, Mmax, x_true, mode)
% function BayesCG() is an implementation of the Bayes Conjugate Gradient
% method proposed in "A Bayesian Conjugate Gradient Method (with
% Discussion)" by Jon Cockayne et al. 
% 
% Input parameters: 
%                   - A - square system matrix
%                   - b - right-hand side
%                   - x_0 - initial guess
%                   - Sigma_0 - prior covariance matrix
%                   - epsilon - tolerance of the solver
%                   - Mmax    - the maximum number of iterations
% Output parameters: 
%                   - x_m - the approximate solution after m iterations
%                   - Sigma_F - the posterior covariance matrix
%                   - nu - scale of posterior covariance
%                   - norm_vec - vector of residual norms

Sigma_F = [];
a_m     = [];
a_m_inv  = [];

N = size(A,1);

S = zeros(N, Mmax);
Y = zeros(N, Mmax);
Y_norm = zeros(N, Mmax);
F = zeros(N, Mmax);
F_norm = zeros(N,Mmax);

E_2 = zeros(Mmax,1);
S_norm = zeros(N, Mmax);

dx       = zeros(Mmax,1);
tr_Sm    = zeros(Mmax,1);
norm_vec = zeros(Mmax,1);

x_m     = x_0;
r_old   = b - A * x_0;
s_m     = r_old;

Q = A * (Sigma_0 *A.');

nu_m   = 0;

s   = s_m / sqrt(s_m.' * Q * s_m);

S(:,1) = s_m ;
S_norm(:,1) = s;
Y_norm(:,1) = A * s;
Y(:,1) = A * s_m;

% main loop
for m =1:Mmax
        
    E_2(m) = s_m.' * Q * s_m;
    alpha_m = r_old.' * r_old / E_2(m); 
    
    dSigma = Sigma_0 * (A.' * s_m);
    
    dx_m    = alpha_m * dSigma;
    x_m     = x_m + dx_m;
    
    r_new    = r_old - A * dx_m;
       
    Sigma_F = [Sigma_F, dSigma / sqrt(E_2(m))]; 
    
    tr_Sm(m,1)   = trace(Sigma_0 - Sigma_F * Sigma_F.');
    
%     Sigma_m = Sigma_0 - Sigma_F * Sigma_F.';
    
    nu_m  = nu_m + alpha_m;%(r_old.' * r_old) ;
    
    if nargin >= 7
        dx(m,1) = norm(x_m - x_true);
    end
        
    norm_vec(m,1) = norm(r_new);
    
    % termination condition
    if norm_vec(m,1) < epsilon
        break
    end
    
    % check whether sequential or batch orthogonalization was selected
    if strcmp(mode, 'sequential')
        beta_m = (r_new.' * r_new) / (r_old.' * r_old);
        s_m = r_new + beta_m * s_m;
    else
        % get candidate search direction
        s_m = r_new; 

        % Q-orthogonalize current search direction w.r.t previous ones
        for j = 1:m
            sj = S(:,j) / sqrt(E_2(j));
            beta_2 = - sj.' * (Q * s_m);
            s_m = s_m + beta_2 * sj;           
        end
    end
    
    % normalize 
    s = s_m / sqrt(s_m.' * Q * s_m);

    S(:,m+1) = s_m;
    Y(:,m+1) = A * s_m;
    S_norm(:, m+1) = s;
    Y_norm(:,m) = A * s;

    a_m = [a_m, s.' * (A * s)/ norm(s)^2];
    a_m_inv = [a_m_inv, s.' * (A * s) / norm(A * s)^2 ];
    
    r_old = r_new;
 end

nu = nu_m / m;

tr_Sm = tr_Sm(1:m);

S = S(:,1:m);
Y = Y(:,1:m);
S_norm = S_norm(:,1:m);
Y_norm = Y_norm(:,1:m);

