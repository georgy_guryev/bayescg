function [L,O] = Cholesky(A, max_itr)
% Input - A spd matrix, max_itr maximum iter
% if max_itr = 0 or too large then set max_itr equals to number of columns of matrix
% Output: L and O = A - LL'

% Initialization
[m, n] = size(A); % Calculate matrix column and rows

if max_itr == 0 || max_itr > n
    max_itr = n;
end

L = [];     % Initializing L
R = A;

% Loop
for i = 1:max_itr
    l = R(:,i)/sqrt(R(i,i));
    L = [L l];
    R = R - l*l';
end

O = A - L*L';
end

