function [trace_Sigma_m] = Krylov_prior_covariance_trace_Hennig(S_full, Y_full, alpha, omega_sq, b)


M = size(S_full,2);

trace_Sigma_m = zeros(M,1);

for i = 1:M
    
    S = S_full(:,1:i);
    Y = Y_full(:,1:i);
    
    [~, ~, ~, Sigma_m, ~] = estimate_solution_uncertainty_Hennig(S,Y, alpha, omega_sq, b);
    trace_Sigma_m(i)      = trace(Sigma_m);
end

