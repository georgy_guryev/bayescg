function A_1d = Laplace_1d(N, a, b)

    h = (b - a) / (N + 1);

    d_1d = ones(N,1);
    A_1d = 1 / h * spdiags([d_1d -2 * d_1d d_1d], [-1 0 1], N, N); 
end 