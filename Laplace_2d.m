function A_2d = Laplace_2d(N, a, b)

    
    I_1d = eye(N);
    A_1d = Laplace_1d(N, a, b);
    
    h = (b - a) / (N + 1);

    A_2d = 1 / h * (kron(A_1d, I_1d) + kron(I_1d, A_1d));
end