function Test_BCG(type, tol, N)


if nargin < 3
    N = 100;
end

switch type 
    case 'Poisson'
        a = 0;
        b = 10;
        A = Laplace_2d(sqrt(N), a, b);
        P = spdiags(spdiags(A, -1:1), -1:1, N, N);
      
    case 'PD_sparse'
        is_sparse = 1;
        [A, ~] = raw_simulate(N, is_sparse, 'exponential');
        [L, ~]  = Cholesky(A, N);
        P = L*L.';
        P(abs(P) < (1e-2 * max(P))) = 0;
        
    case 'PD_dense'
        is_sparse = 0;
        [A, ~]  = raw_simulate(N, is_sparse, 'exponential');
        I = find(A == 0);
        [L, ~]  = Cholesky(A, N);
        P      = L*L.';
        P(I) = 0;

end


Mmax = 200;

b   = rand(N,1);
x_0 = zeros(N,1);

%% find the exact solution 

x_true = A \ b;

%% solve with Sigma = eye 

Sigma_I = eye(N);

% [dx_I, tr_Sigma_I, ~, norm_vec_I] = BayesCG(A,b,x_0, Sigma_I, tol, Mmax, x_true);

[x_m, dx_I, tr_Sigma_I, Sigma_F, ~, norm_vec_I, S, S_norm, Y, ~, a_m, a_m_inv] =  BayesCG(A, b, x_0, Sigma_I, tol, Mmax, x_true, 'batch');

M  = size(S,2);

alpha  = sum(abs(x_m(1:end-1) - x_m(2:end)));
nu = alpha / trace(Sigma_I - Sigma_F * Sigma_F.');

[~, Sigma_inv_sqrt] = estimate_solution_uncertainty_Cockayne(Sigma_I, Sigma_F, M, nu);

Z_cockayne = Sigma_inv_sqrt * (x_true - x_m);


figure();
h = histogram(Z_cockayne);
h.Normalization = 'pdf';
hold on;
p = @(x) 1 / sqrt(2 * pi) * exp(-x.^2/2);
x_norm = -5:0.01:5;
plot(x_norm, p(x_norm), 'LineWidth', 2);
hold off;
xlabel('$\mathbf{Z_i}$', 'interpreter', 'latex');
ylabel('\bf PDF', 'interpreter', 'latex');
legend('$\mathbf{Z = \Sigma_m^{-1/2}(X_{true} - X_{BCG})}$', '\bf Standard  Gaussian', 'interpreter', 'latex');


%% solve with Sigma = A^(-1) 

Sigma_A = eye(N) / (A);

% [dx_A, tr_Sigma_A, ~, norm_vec_A] = BayesCG(A,b,x_0, Sigma_A, tol, Mmax, x_true);
[x_m, dx_A, tr_Sigma_A, Sigma_F, ~, norm_vec_A, S_A, S_norm, Y_A, ~, a_m, a_m_inv] =  BayesCG(A, b, x_0, Sigma_A, tol, Mmax, x_true, 'batch');


M_A  = size(S_A,2);

alpha  = sum(abs(x_m(1:end-1) - x_m(2:end)));
nu = alpha / trace(Sigma_A - Sigma_F * Sigma_F.');

[~, Sigma_inv_sqrt] = estimate_solution_uncertainty_Cockayne(Sigma_A, Sigma_F, M_A, nu);


Z_cockayne = Sigma_inv_sqrt * (x_true - x_m);

figure();
h = histogram(Z_cockayne);
h.Normalization = 'pdf';
xlabel('$\mathbf{Z_i}$', 'interpreter', 'latex');
ylabel('\bf PDF', 'interpreter', 'latex');
legend('$\mathbf{Z = \Sigma_m^{-1/2}(X_{true} - X_{BCG})}$', 'interpreter', 'latex');

%% solve with Sigma = (P*P.')^(-1) 

Sigma_P = eye(N) / (P.'*P);

% [dx_P, tr_Sigma_P, ~, norm_vec_P] = BayesCG(A,b,x_0, Sigma_P, tol, Mmax, x_true);
[x_m, dx_P, tr_Sigma_P, Sigma_F, ~, norm_vec_P, S, S_norm, Y, ~, ~, a_m_inv] =  BayesCG(A, b, x_0, Sigma_P, tol, Mmax, x_true, 'batch');


M  = size(S,2);

alpha  = sum(abs(x_m(1:end-1) - x_m(2:end)));
nu = alpha / trace(Sigma_P - Sigma_F * Sigma_F.');

[~, Sigma_inv_sqrt] = estimate_solution_uncertainty_Cockayne(Sigma_P, Sigma_F, M, nu);


Z_cockayne = Sigma_inv_sqrt * (x_true - x_m);


figure();
h = histogram(Z_cockayne);
h.Normalization = 'pdf';
xlabel('$\mathbf{Z_i}$', 'interpreter', 'latex');
ylabel('\bf PDF', 'interpreter', 'latex');
legend('$\mathbf{Z = \Sigma_m^{-1/2}(X_{true} - X_{BCG})}$', 'interpreter', 'latex');

%% compute trace 

[eig_Ainv] = eig(Sigma_A);
alpha = 0.9 * min(eig_Ainv);

phi      = fit_polynomial(A, a_m, N, size(a_m,2), 2);
omega_sq = 1 / (4 * phi);

[tr_S_Krylov] = Krylov_prior_covariance_trace_Hennig(S_A, Y_A, alpha, omega_sq, b);

%% preprocess results for visualisation



res_norms_mat = [norm_vec_I, norm_vec_A, norm_vec_P];
tr_Sm_norms   = {tr_Sigma_I / trace(Sigma_I),...
                 tr_Sigma_A / trace(Sigma_A),...
                 tr_Sigma_P / trace(Sigma_P),...
                 abs(tr_S_Krylov(1:end)) / trace(Sigma_A)};
             
dx_norms_mat  = [dx_I, dx_A, dx_P];

cell_legends  = {'\Sigma_0 = I', ...
                 '\Sigma_0 = A^{-1}',...
                 '\Sigma_0 = (P^TP)^{-1}',...
                 'Krylov prior / Hennig UQ'};

ylabel_dx  = '$$\bf \Vert x^* - x_m \Vert_2$$';
ylabel_Sm  = '$$\bf trace(\Sigma_m) / trace(\Sigma_0)$$';
ylabel_res = '$$\bf \Vert b - Ax_m \Vert_2$$';

%% plot decay of the residual



plot_residual_norms(dx_norms_mat, b, cell_legends, ylabel_dx);
plot_uncertainty(tr_Sm_norms, cell_legends,ylabel_Sm, 'lin');
plot_uncertainty(tr_Sm_norms, cell_legends,ylabel_Sm, 'log');
plot_residual_norms(res_norms_mat, b, cell_legends,ylabel_res);



