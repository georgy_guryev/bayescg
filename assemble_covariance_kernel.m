function Kxx = assemble_covariance_kernel(kernel, data, eta)

N   = size(data,1);
Kxx = zeros(N);


for i = 1:N
    for j =1:N
        Kxx(i,j) = kernel(data(i,:), data(j,:), eta);     
    end
end
