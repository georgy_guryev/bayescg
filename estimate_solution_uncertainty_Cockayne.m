function [Sigma_m, Sigma_inv_sqrt] = estimate_solution_uncertainty_Cockayne(Sigma_0, Sigma_F, M, nu)

Sigma_m = (Sigma_0 - Sigma_F * Sigma_F.') * nu;

Sigma_m = (Sigma_m + Sigma_m') / 2;

% get posterior precision of the solution
[U,D] = eig(Sigma_m);

U_hat = U(:,M+1:end);
d_vec = diag(D);
d_vec = sqrt(d_vec(M+1:end));
D_hat = real(diag(1 ./ d_vec));

Sigma_inv_sqrt = U_hat * D_hat * U_hat.';

Sigma_inv_sqrt = (Sigma_inv_sqrt + Sigma_inv_sqrt') / 2; 

