function [x_hennig, Hm, Wm, Sigma, Sigma_inv] = estimate_solution_uncertainty_Hennig(S,Y, alpha, omega_sq, b)

[N,M] = size(S);

I = eye(N);

Hm = alpha * I + (S - alpha * Y) /(Y.' * S - alpha * (Y.' *Y)) * (S - alpha * Y).';
% Wm = S / (S.' * Y) * S.' + (I - F_norm * F_norm.') * omega_sq * (I - F_norm * F_norm.') - Hm;
Wm = S / (S.' * Y) * S.' + (I - Y / (Y.' * Y)* Y.') * omega_sq * (I - Y / (Y.' * Y)* Y.') - Hm;



% compute point estimate according to Hennig
x_hennig   = Hm * b;

% get posterior covariance matrix
Sigma = 1 / 2 * (Wm * (b.' * (Wm * b)) + (Wm * b) * (b.' * Wm));
Sigma = (Sigma + Sigma') / 2;

% get posterior precision of the solution
[U,D] = eig(Sigma);

U_hat = U(:,M+1:end);
d_vec = diag(D);
d_vec = sqrt(d_vec(M+1:end));
D_hat = diag(1 ./ d_vec);

Sigma_inv = U_hat * D_hat * U_hat.';

Sigma_inv = (Sigma_inv + Sigma_inv') / 2; 

