function [phi] = fit_polynomial(A, a_m, N, M, order)
% regularized linear regression 

offset = round(0.2*M);

X = vandermonde(1:N, order);
V = vandermonde(offset:M, order);
c = (V.' * V + 0.01 .* eye(size(M-offset+1))) \ V.'* a_m(offset:end).';

Y = X * c;

eig_A = eig(A);

eig_A = sort(eig_A, 'ascend');

figure(); semilogy(abs(a_m)); 
hold on; 
semilogy(Y(1:N));
semilogy(eig_A(end:-1:1));
title('The polynomial approximant to eigenvalue decay');
xlabel('Iteration count/number of visited eigenvalues');
ylabel('True and approximate eigenvalues');
legend('Rayleigh quotients from BCG', 'fit polynomial', 'true eigenvalues');

phi  = mean(Y(M+1:N));
