%% Experiment on structures of eigenvalues

N = 2000;
[A, ~] = raw_simulate(N, 0, "exponential");
b = 2*randn(N,1);
x_0 = zeros(N,1);
A_inv = inv(A);
I = eye(N);
Sigma_0 = A_inv;

[x_m, dx, tr_Sm, nu, norm_vec, S, S_norm, Y, F, F_norm, a_m, a_m_inv, omega_sqr] =  BayesCG(A, b, x_0, Sigma_0, 1e-8, 1500, [], 'batch');

%% inverser case A^-1

big_elval = S(:,1).' * A * S(:,1) / norm(S(:,1))^2;
alpha = 0.9 / big_elval;
Hm = alpha * I + (S_norm - alpha * Y) / (Y.' * S_norm - alpha * (Y.' * Y)) * (S_norm - alpha * Y).';

x_cockayne = x_m;
x_hennig   = Hm * b;

norm(b - A * x_cockayne) / norm(b)
norm(b - A * Hm * b) / norm(b)

%% 
omega_sq = 68; %1.84 * 1e-8; %;
Wm = S_norm / (S_norm.' * Y) * S_norm.' + (I - F_norm * F_norm.') * omega_sq * (I - F_norm * F_norm.') - Hm;

% define covariance for H*b = A_inv * b

Hmb = Hm * b;
Sigma_x = 1 / 2 * (Wm * (b.' * (Wm * b)) + (Wm * b) * (b.' * Wm));

