function h = plot_residual_norms(res_norms_mat, rhs, cell_legends, title_str)
% function plots residual norms (in log scale)

[~,N] = size(res_norms_mat);

% check norm matrix is empty
if N < 1
    error('plot_residual_norms(): the input residual matrix is empty! \n');
end

rhs_norm = norm(rhs);

h = figure(); 

loglog(res_norms_mat(:,1) / rhs_norm, '-x');
hold on; 

for i = 2:N
    loglog(res_norms_mat(:,i) / rhs_norm, '-o');
end

legend(cell_legends{:});
hold off;
ylabel(title_str, 'interpreter', 'latex');
xlabel('Iteration number');