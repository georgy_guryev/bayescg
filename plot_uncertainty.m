function h = plot_uncertainty(res_norms_mat, cell_legends, title_str, mode)
% function plots residual norms (in log scale)

[~,N] = size(res_norms_mat);

% check norm matrix is empty
if N < 1
    error('plot_residual_norms(): the input residual matrix is empty! \n');
end

h = figure(); 


switch mode
    
    case 'lin'
        plot(res_norms_mat{1} , '-x');
        hold on;
        
        for i = 2:N
                plot(res_norms_mat{i}, '-o');
        end
        
    case 'log'
        
        semilogy(res_norms_mat{1}, '-x');
        hold on;
        
        for i = 2:N
            semilogy(res_norms_mat{i}, '-o');
        end
end



legend(cell_legends{:});
hold off;
ylabel(title_str, 'interpreter', 'latex');
xlabel('Iteration number');