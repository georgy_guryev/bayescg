function [Kab] =   radial_kernel(a,b, eta)

if size(a,2) ~= size(b,2) 
    error('Dimension missmatch \n');
end

if size(a,2) ~= 21
    error('A 21-dim vector expected \n')
end

Kab = exp(- 1/ (2 * eta^2) * norm(a-b)^2 );


