function [mat, eigval] = raw_simulate(N,is_sparse,dist,rate,density)
% To simulate large spd matrices with eigenvalues sampled from Exp(gamma): 
% if is_dense = 0 then return a dense matrix, (density means how dense is a matrix)
% if is_dense = 1 then return a sparse matrix.

% if gamma is not specified then set gamma = 1.

% First simulate the eigenvalues
if strcmp(dist, 'exponential')
    if ~exist('rate','var')
        rate = 2;
    end
    eigval = exprnd(rate, N, 1);
elseif strcmp(dist, 'uniform')
    eigval = rand(N, 1);
end

% then simulate matrices
if is_sparse == 0
    [Q,~] = qr(randn(N,N));
    mat = Q * diag(eigval) * Q.';
elseif is_sparse == 1
    if ~exist('density', 'var')
        density = 0.5*rand;
    end
    mat = sprandsym(N,density,eigval);
end
end


