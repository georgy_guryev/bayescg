% test Henning's discussion paper

filename = 'sarcos_inv.mat';

N     = 1000;
M     = 100; 
sigma = 0.1;
eta   = 2; 

%% load data file 
load(filename);

% preprocess data
input_data = sarcos_inv(:,1:21);
rhs        = sarcos_inv(:,22);

% define system and rhs
A =  setup_sacros_experiment(input_data, eta, sigma, N); 
b = rhs(1:N,1);

% get 'true' solution
x_true = A \ b;

% invert matrix 
A_inv = inv(A);
I = eye(N);


%% 

N = size(A,1);

% set initial guess to zero
x_0 = zeros(N,1);

% set initial covariance matrix to A^-1
Sigma_0 = A_inv;

% run Bayes CG
[x_cockayne, dx, tr_Sm, Sigma_F, nu, norm_vec, S, S_norm, Y, Y_norm, a_m, a_m_inv] =  BayesCG(A, b, x_0, Sigma_0, 1e-10, M, x_true, 'batch');


%% pretend that we have an eigenvalue oracle

[eig_Ainv] = eig(A_inv);
alpha = 0.9 * min(eig_Ainv);


%% Studying the decay of the eigenvalues

x = (1:length(a_m)).';
y = log(a_m);
ell = 10; % length-scale of parameter
% can use matern32
gprMdl = fitrgp(x, y, 'KernelFunction', 'squaredexponential', 'KernelParameters', [ell*std(x); std(y)/sqrt(2)],'BasisFunction', 'linear', 'FitMethod', 'sr', 'PredictMethod', 'fic','Standardize',0);

%% Prediction

xpred = (1:N).';
ypred = predict(gprMdl, xpred);
% ypred_resub = resubPredict(gprMdl);
% plot(x,y,'b.'); hold on; plot(x,ypred_resub,'r','LineWidth',1.5); hold off
plot(x,y,'b.'); hold on; plot(xpred,ypred,'r','LineWidth',1.5); hold off
title('The GP approximant to eigenvalue decay');
xlabel('Iteration count/number of visited eigenvalues');
ylabel('True and approximate eigenvalues');
legend('Rayleigh quotients', 'approximant from GP');

% obtaining omega
% phi = exp(1/(N-length(a_m)) * sum(ypred(length(a_m)+1:end)));
phi = 1/(N-length(a_m)) * sum(exp(ypred(length(a_m)+1:end)));
% phi = sigma^2;


phi = fit_polynomial(A, a_m, N, M, 1);


%% inverse case A^-1

% phi = 0.0146;
omega_sq = 1 / (9 * phi);

% estimate uncertaintry according to Hennig (identical result is achieved
% by Krylov subspace prior in Cockayne)
[x_hennig, Hm, Wm, Sigma, Sigma_inv] = estimate_solution_uncertainty_Hennig(S,Y, alpha, omega_sq,b);

% form standarized variable
Z = Sigma_inv * (x_true - x_hennig);


figure();
h = histogram(Z);
h.Normalization = 'pdf';
hold on;
p = @(x) 1 / sqrt(2 * pi) * exp(-x.^2/2);
x_norm = -5:0.01:5;
plot(x_norm, p(x_norm), 'LineWidth', 2);
hold off;
xlabel('$\mathbf{Z_i}$', 'interpreter', 'latex');
ylabel('\bf PDF', 'interpreter', 'latex');
legend('$\mathbf{Z = \Sigma_m^{-1/2}(X_{true} - X_{BCG})}$', '\bf Standard  Gaussian', 'interpreter', 'latex');

%%

% the BCG covariance estimate according to Cockayne
[Sigma_m, Sigma_inv_sqrt] = estimate_solution_uncertainty_Cockayne(Sigma_0, Sigma_F, M, nu);

% form standarized variable
Z_cockayne = Sigma_inv_sqrt * (x_true - x_cockayne);


figure();
h = histogram(Z_cockayne);
h.Normalization = 'pdf';
xlabel('$\mathbf{Z_i}$', 'interpreter', 'latex');
ylabel('\bf PDF', 'interpreter', 'latex');
legend('$\mathbf{Z = \Sigma_m^{-1/2}(X_{true} - X_{BCG})}$', '\bf Standard  Gaussian', 'interpreter', 'latex');


%% relative accuracies of both methods


norm(b - A * x_cockayne) / norm(b)
norm(b - A * Hm * b) / norm(b)




