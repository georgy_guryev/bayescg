% test Henning's discussion paper, fitting Gaussian process


%% load data file 

% forming matrix
filename = 'sarcos_inv.mat';
load(filename);
input_data = sarcos_inv(:,1:21);
rhs        = sarcos_inv(:,22);

% Initialisation
N = 1000;
sigma = 0.1;
eta   = 2; 

% Forming test matrix
K = assemble_covariance_kernel(@radial_kernel, input_data(1:N,:), eta);
A = K + sigma^2 * eye(size(K,1));
b = rhs(1:N,1);
A_inv = inv(A);
I = eye(N);

%% Running BayesCG with sanity check

% Running CG
x_0 = zeros(N,1);
Sigma_0 = A_inv;
[x_m, dx, tr_Sm, nu, norm_vec, S, S_norm, Y, Y_norm, F, F_norm, a_m, a_m_inv, omega_sqr] =  BayesCG(A, b, x_0, Sigma_0, 1e-10, 500, [], 'batch');

% forming inverse matrix
[eig_Ainv] = eig(A_inv);
alpha = 0.99* min(eig_Ainv);
omega_sq = 68; %1.84 * 1e-8; %;
Hm = alpha * I + (S_norm - alpha * Y) / (Y.' * S_norm - alpha * (Y.' * Y)) * (S_norm - alpha * Y).';
Wm = S_norm / (S_norm.' * Y) * S_norm.' + (I - F_norm * F_norm.') * omega_sq * (I - F_norm * F_norm.') - Hm;

% computing errors
x_cockayne = x_m;
x_hennig   = Hm * b;
norm(b - A * x_cockayne) / norm(b)
norm(b - A * Hm * b) / norm(b)

%% comparing matrix norm difference for A and A_inv
beta = 1; 
omega = 0.02;
Am = beta * eye(N) + ((Y - beta * S_norm) / (S_norm.' * Y)) * Y.';
Amm  = beta * eye(N) + ((Am * S_norm - beta * S_norm) / (S_norm.' * Am * S_norm)) * (Am * S_norm).';
WA_m = omega * (I - S_norm / (S_norm.' * S_norm) * S_norm.');
Am_d = diag(diag(Am));
Ad = diag(diag(A));
Am_nd = Am - Am_d;
A_nd  = A - Ad;
norm(Am_d - Ad) / norm(Ad)
norm(Am_nd - A_nd) / norm(A_nd)

Hm_d = diag(diag(Hm));
Hd = diag(diag(A_inv));
Hm_nd = Hm - Hm_d;
H_nd  = A_inv - Hd;
norm(Hm_d - Hd) / norm(Hd)
norm(Hm_nd - H_nd) / norm(H_nd)

%% Studying the decay of the eigenvalues
x = (1:length(a_m)).';
y = log(a_m);
ell = 10; % length-scale of parameter
% can use matern32
gprMdl = fitrgp(x, y, 'KernelFunction', 'squaredexponential', 'KernelParameters', [ell*std(x); std(y)/sqrt(2)],'BasisFunction', 'linear', 'FitMethod', 'sr', 'PredictMethod', 'fic','Standardize',0);

%% Prediction
xpred = (1:N).';
ypred = predict(gprMdl, xpred);
% ypred_resub = resubPredict(gprMdl);
% plot(x,y,'b.'); hold on; plot(x,ypred_resub,'r','LineWidth',1.5); hold off
plot(x,y,'b.'); hold on; plot(xpred,ypred,'r','LineWidth',1.5); hold off

% obtaining omega
% phi = exp(1/(N-length(a_m)) * sum(ypred(length(a_m)+1:end)));
phi = 1/(N-length(a_m)) * sum(exp(ypred(length(a_m)+1:end)));
% phi = sigma^2;

% forming variance matrix
Wm = S_norm / (S_norm.' * Y) * S_norm.' + (I - F_norm * F_norm.') * (1/phi) * (I - F_norm * F_norm.') - Hm;
Hmb = Hm * b;
Sigma_x = 1 / 2 * (Wm * (b.' * (Wm * b)) + (Wm * b) * (b.' * Wm));
trace(Sigma_x)


