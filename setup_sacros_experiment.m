function A = setup_sacros_experiment(input_data, eta, sigma, N)
% 

K = assemble_covariance_kernel(@radial_kernel, input_data(1:N,:), eta);
A = K + sigma^2 * eye(size(K,1));
