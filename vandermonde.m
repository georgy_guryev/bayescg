function X = vandermonde(x,p)

m = max(size(x));
n = p;


X = zeros(m,n);


for i = 1:m
    for j = 1:n
        X(i,j) = x(i)^(j-1);
    end
end